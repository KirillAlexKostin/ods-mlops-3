#!/bin/bash

minikube kubectl -- delete -f k8s/nginx-ingress.yaml
sleep 1
minikube kubectl -- delete -f k8s/nginx-service.yaml
sleep 1
minikube kubectl -- delete -f k8s/nginx-deployment.yaml
sleep 1
minikube kubectl -- delete -f k8s/nginx-configmap.yaml
sleep 1
minikube delete
