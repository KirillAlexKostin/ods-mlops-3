import os


# ROOT_DIR is needed to make it possible to run snakemake from any directory of the project
ROOT_DIR = os.getenv('ROOT_DIR', '/app')
DATASETS = ['X', 'y']
SUBSETS = ['train', 'test']


def delete_file(file_path: str):
    from pathlib import Path

    path = Path(file_path)
    path.unlink()


rule all:
    input:
        expand('{root_dir}/data/cancer_data/Cancer_Data_Read.csv', root_dir=ROOT_DIR),
        expand('{root_dir}/data/cancer_data/Cancer_Data_{dataset}{subset}.csv', root_dir=ROOT_DIR, dataset=DATASETS, subset=SUBSETS),
        expand('{root_dir}/data/cancer_data/Cancer_Data_{dataset}{subset}STD.csv', root_dir=ROOT_DIR, dataset=DATASETS, subset=SUBSETS),
        expand('{root_dir}/data/cancer_data/logreg.pkl', root_dir=ROOT_DIR),
        expand('{root_dir}/data/cancer_data/rf_cls.pkl', root_dir=ROOT_DIR)


rule read_data:
    input: expand('{root_dir}/data/cancer_data/Cancer_Data.csv', root_dir=ROOT_DIR)
    output: expand('{root_dir}/data/cancer_data/Cancer_Data_Read.csv', root_dir=ROOT_DIR)
    shell: 'jupyter execute notebooks/cancer_research/brest_cancer_read.ipynb'


rule preprocess_data:
    input: expand('{root_dir}/data/cancer_data/Cancer_Data_Read.csv', root_dir=ROOT_DIR)
    output: expand('{root_dir}/data/cancer_data/Cancer_Data_{dataset}{subset}.csv', root_dir=ROOT_DIR, dataset=DATASETS, subset=SUBSETS)
    threads: 2
    shell: 'jupyter execute notebooks/cancer_research/brest_cancer_preprocess.ipynb'


rule preprocess_data_std:
    input: expand('{root_dir}/data/cancer_data/Cancer_Data_Read.csv', root_dir=ROOT_DIR)
    output: expand('{root_dir}/data/cancer_data/Cancer_Data_{dataset}{subset}STD.csv', root_dir=ROOT_DIR, dataset=DATASETS, subset=SUBSETS)
    threads: 2
    shell: 'BCP_STANDARDIZE=1 jupyter execute notebooks/cancer_research/brest_cancer_preprocess.ipynb'


rule train_logreg:
    input: expand('{root_dir}/data/cancer_data/Cancer_Data_{dataset}{subset}STD.csv', root_dir=ROOT_DIR, dataset=DATASETS, subset=SUBSETS)
    output: expand('{root_dir}/data/cancer_data/logreg.pkl', root_dir=ROOT_DIR)
    threads: 2
    shell: 'jupyter execute notebooks/cancer_research/brest_cancer_logreg.ipynb'


rule train_model_rfcls:
    input: expand('{root_dir}/data/cancer_data/Cancer_Data_{dataset}{subset}.csv', root_dir=ROOT_DIR, dataset=DATASETS, subset=SUBSETS)
    output: expand('{root_dir}/data/cancer_data/rf_cls.pkl', root_dir=ROOT_DIR)
    threads: 2
    shell: 'jupyter execute notebooks/cancer_research/brest_cancer_randomforest.ipynb'


rule clean:
    run:
        for i in range(len(rules.all.input)):
            delete_file(str(rules.all.input[i]))
