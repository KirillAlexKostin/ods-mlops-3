---
title: "Snakemake"
engine: jupyter
---

В проекте с помощью системы управления workflow [Snakemake](https://snakemake.readthedocs.io/en/stable/) реализованы автоматизированные пайплайны проведения исследований.

На текущий момент реализован workflow, включающий в себя 2 пайплайна. Общая схема (DAG) пайплайна представлен ниже.

```{python}
!ROOT_DIR=../ snakemake --snakefile ../Snakefile --dag 2> /dev/null | dot -Tpng > snakemake_dag.png
```

*Note: альтернативный способ - использовать `snakemake` с файлом `src/cancer_scripts/Snakefile`, который работает с python-скриптами, использущими [Hydra](https://hydra.cc/), например, так: `!cd ../src/cancer_scripts && ROOT_DIR=../../ snakemake --dag 2> /dev/null | dot -Tpng > snakemake_dag.png && mv snakemake_dag.png ../../docs/ && cd ../../`.*

![Snakemake DAG](snakemake_dag.png)

Первый пайплайн - получение модели RandomForestClassifier, обученной на входных данных `Cancer_Data.csv`. Он включает с себя этапы считывания данных (*read_data*), подготовки данных без их нормализации с помощью StandardScaler (*preprocess_data*), а также непосредственно этап обучения модели (*train_model_rfcls*).

Второй пайплайн - получение модели LogisticRegression, обученной на тех же входных данных `Cancer_Data.csv`. Он базируется на тех же входных данных, получаемых на этапе *read_data*, но для нормализации данных проходит через этап *preprocess_data_std*, где производится нормализация данных с помощью StandardScaler, последним этапом пайплайна является *train_logreg* этап, на выходе которого создаётся файл модели логистической регрессии.
