# ODS MLOps 3

Репозиторий для курса [Open Data Science](https://ods.ai/) MLOps и production в DS исследованиях 3.0.

[[_TOC_]]

## 1 Ведение проекта

Методология ведения проекта и описание того, как можно сделать вклад в проект, описаны в файле [CONTRIBUTING.md](./CONTRIBUTING.md).

## 2 Первоначальная настройка

**Внимание:** рекомендуемый способ работы с репозиторием для разработки - использование dev docker-контейнера, для настройки работы с репозиторием с помощью docker-контейнера, см. п. 3.

1. Для настройки и работы с репозиторием рекомендуется использовать ОС Linux, например, Ubuntu или WSL.
2. Установите Python 3.11. В Ubuntu, например, это можно сделать с помощью подключения стороннего репозитория системных пакетов:
    ```bash
    sudo add-apt-repository ppa:deadsnakes/ppa -y \
    sudo apt update \
    sudo apt install python3.11 python3.11-dev python3.11-venv python3-pip
    ```
3. Создайте виртуальное окружение python с помощью `venv` и активируйте его:
    ```bash
    python3.11 -m venv .ods-mlops-env \
    source .ods-mlops-env/bin/activate
    ```
    Прим.: виртуальное окружение можно создать как внутри локального репозитория, так и вне него; стоит обратить внимание на то, чтобы не закоммитить папку с виртуальным окружением и его содержимым в репозиторий (обращайте внимание на пути к виртуальному окружению перед его активацией и на правила в файле `.gitignore`).
4. Установите в виртуальное окружение `ruff` и `pre-commit`. Можно воспользоваться командой `pip install ruff==0.3.5 pre-commit==3.7.0`.
5. Настройка `ruff`. Настройки расположены в файле `pyproject.toml`. Если для разработки используется IDE VSCode, то можно установить Extension ruff, а также настроить файл `.vscode/settings.json` следующим образом:
    ```json
    {
        "notebook.formatOnSave.enabled": true,
        "notebook.codeActionsOnSave": {
            "notebook.source.fixAll": "explicit",
            "notebook.source.organizeImports": "explicit"
        },
        "[python]": {
            "editor.formatOnSave": true,
            "editor.defaultFormatter": "charliermarsh.ruff",
            "editor.codeActionsOnSave": {
                "source.fixAll": "explicit",
                "source.organizeImports": "explicit"
            }
        }
    }
    ```
6. Настройка `pre-commit`. Настройки расположены в файле `.pre-commit-config.yaml`. Перед первым использованием необходимо выполнить следующие команды, находясь в репозитории проекта:
    ```bash
    pre-commit install \
    pre-commit
    ```

## 3 Настройка docker

### 3.1 Необходимые условия

Для настройки docker, необходимо установить [Docker Engine](https://docs.docker.com/engine/install/), а также настроить доступ к docker без sudo: [Manage Docker as non-root user](https://docs.docker.com/engine/install/linux-postinstall/#manage-docker-as-a-non-root-user).

### 3.2 Сборка docker-образа

В репозитории предусмотрена сборка docker-образа для разработки. Этот docker-образ подразумевает настройку окружения для разработки с помощью [Poetry](https://python-poetry.org/), а также создание пользователя для разработки в docker-контейнере. В docker-образ не копируются файлы исходного кода, вместо этого подразумевается монтирование рабочей директории проекта внутрь docker-контейнера и ведение разработки в этом docker-контейнере.

Docker-образ также поразумевает, что разработка может производиться с помощью IDE VSCode и расширения для него под названием Remote Development. **Внимание:** для удобства работы с удалённым репозиторием в dev docker-контейнере рекомендуется настроить ссылку на удалённйы репозиторий через https или использовать специальный Access Token. Для этого в Dockerfile прописывается спецификация docker volume для хранения настроек VSCode, чтобы они сохранялись после перезапуска контейнера. Для обеспечения этой возможности необходимо создать docker volume с помощью следующей команды:

```bash
docker volume create ods-mlops-vscode-settings
```

Для сборки docker-образа воспользуйтесь командой:

```bash
docker build \
--build-arg USER_ID=$(id -u) \
--build-arg GROUP_ID=$(id -g) \
-t ods-mlops-3 \
-f docker/Dockerfile.dev \
.
```

Результатом выполнения команды выше будет docker-образ `ods-mlops-3:latest`, в котором будет создан пользователь `ods-user` с UID и GID пользователя хоста, запускающего сборку docker-образа.

### 3.3 Запуск dev docker-контейнера

Запуск собранного docker-контейнера для разработки осуществляется с помощью следующей команды (выполняйте команду, находясь в корне рабочей директории проекта):

```bash
docker run --rm -d \
--mount type=bind,source=$(pwd),destination=/app \
--mount type=volume,source=ods-mlops-vscode-settings,destination=/home/ods-user/.vscode-server \
--name ods-mlops-dev \
-p 8888:8888 \
ods-mlops-3:latest
```

Результатом выполнения команды выше будет запуск docker-контейнера в режиме демона, к которому после этого можно подключиться либо из терминала, либо с помощью VSCode и плагина Remote Development.

Для подключения из терминала воспользуйтесь командой:

```bash
docker exec -it ods-mlops-dev /bin/bash
```

Подключение с помощью VSCode зависит от того, где запущен контейнер. Если контейнер запущен локально, то воспользуйтесь командой в VSCode "Dev Containers: Attach to Running Container...". Если контейнер запущен на удалённой машине, то сначала необходимо подключиться к этой уделённой машине по ssh (необходима предварительная настройка ssh-соединения с удалнной машиной) - команда в VSCode "Remote-SSH: Connect Current Window to Host...", а затем выполнить команду "Remote Explorer: Focus on Dev Containers View...". Откроется визуальный обозреватель зупащенных контейнеров на удалённой машине, в котором появится опция подключения к запущенному контейнеру.

Если было выполнено подключение из терминала и разработка производится с помощью Jupyter Notebook, то внутри контейнера существует возможность запустить Jupyter Server с помощью следующей команды:

```bash
./run_jupyter.sh
```

Результатом выполнения команды выше будет запуск Jupyter Notebook внутри docker-контейнера, к которому можно будет подключиться, скопировав URL в выводе работы контейнера. Все созданные файлы при работе с Jupyter Notebook будут сохранены в рабочей директории репозитория с правами доступа, соответствующими правам доступа пользователя хоста.

Далее выведенную строку с адресом для подключения к Jupyter Notebook можно вставить в браузер, а для настройки ssh-тунеля, в случае, если Jupyter Server находится на удалённой машине можно пробросить следующим образом, выполнив команду на локальной машине:

```bash
ssh -N -L [jupyter_port]:localhost:[jupyter_port] [username]@[hostname|IP]
```

Где [jupyter_port] - это порт, отображающийся в строке подключения к Jupyter Notebook, [username] - имя пользователя на удалённой машине (пользователь хоста), [hostname|IP] - адрес удалённой машины (тот, который использовался для подключения по SSH).

## 4 Clear ML

### 4.1 Необходимые условия

Для запуска и работы с Clear ML необходимо установить docker-compose: [Install Compose standalone](https://docs.docker.com/compose/install/standalone/).

### 4.2 Запуск Clear ML Server

Для запуска Clear ML Server в проекте используется docker-compose и файл конфигурации `infra/docker-compose.yml`. Для запуска необходимо выполнить команду:

```bash
docker-compose -f infra/docker-compose.yml up -d
```

Результатом выполнения команды будет запуск набора сервисов Clear ML Server в фоновом режиме. Для доступа к Web UI необходимо открыть адрес `http://localhost:8080` на хостовой машине. Для доступа к специальным функциям Clear ML, например, для использования агентов, укажите при запуске вышеприведённого скрипта переменные окружения `CLEARML_HOST_IP`, `CLEARML_AGENT_GIT_USER`, `CLEARML_AGENT_GIT_PASS`. При возникновении проблем можно обратиться к официальной [документации](https://clear.ml/docs/latest/docs/deploying_clearml/clearml_server_linux_mac/) настройки и развёртывания Clear ML Server.

При этом, важно, что при запуске создаётся несколько docker network'ов: `infra_backend` и `infra_frontend`. Docker network важны, если Clear ML Client, который запускается из python-скриптов проекта, будет запускаться внутри другого контейнера, например, внутри dev docker-контейнера текущего проекта.

Для остановки Clear ML Server необходимо использовать команду:

```bash
docker-compose -f infra/docker-compose.yml down
```

При этом, созданные при запуске docker network удалятся.

### 4.3 Настройка Clear ML Client

Для начала работы с Clear ML необходимо с помощью графического интерфейса Web UI создать credentials. Для этого необходимо перейти в **SETTINGS** -> **WORKSPACE** и выбрать опцию **Create new crdentials**. В открывшемся всплывающем окне необходимо куда-то скопировать созданный код конфигурации.

В скопированном коде конфигурации может быть необходимо поменять адреса `web_server`, `api_server` и `files_server`. Если Client (python-скрипты, использующие Clear ML) будут запускаться из другого docker-контейнера, который будет находиться в том же docker network, то адреса необходимо поменять на следующе:

```text
api {
  web_server: http://clearml-webserver:80
  api_server: http://clearml-apiserver:8008
  files_server: http://clearml-fileserver:8081
  credentials {
    "access_key" = "XXX"
    "secret_key" = "XXXXX"
  }
}
```

В приведённом выше фрагменте конфигурации прописанные адреса основываются на именах запущенных docker-контейнеров сервисов Clear ML.

Если планируется запуск Client (python-скриптов, использующих Clear ML) из dev docker-контейнера, то его необходимо запустить после запуска Clear ML Server с помощью немного изменённой команды:

```bash
docker run --rm -d \
--mount type=bind,source=$(pwd),destination=/app \
--mount type=volume,source=ods-mlops-vscode-settings,destination=/home/ods-user/.vscode-server \
--name ods-mlops-dev \
-p 8888:8888 \
--network infra_frontend \
ods-mlops-3:latest
```

Команда запуска dev docker-контейнера отличается от команды, описанной в п. 3.3, указанием docker network, необходимой для доступа к запущенным сервисам Clear ML Server.

Наконец, для окончания настройки Clear ML Client необходимо выполнить следующую команду в терминале dev docker-контейнера:

```bash
poetry run clearml-init
```

В промпте ввести скопированную и изменнную конфигурацию для подключения к Clear ML Server. После этого можно запускать любые Python-скрипты, использующие Clear ML в проекте (например, `src/amazon_scripts_clearml/run_clearml_experiment.py`). При возникновении проблем можно обратиться к официальной [документации](https://clear.ml/docs/latest/docs/getting_started/ds/ds_first_steps) настройки Clear ML.

## 5 Nginx в Kubernetes кластере

### 5.1 Необходимые условия

Установленный в локальной системе Minikube - см. инструкцию на [сайте](https://minikube.sigs.k8s.io/docs/start/?arch=/linux/x86-64/stable/binary+download). В данном проекте подразумевается, что minikube доступен как исполняемая команда в командной оболочке bash в локальной Linux-системе с установленным Docker Engine.

Проверить корректность установки Minikube можно с помощью команды `minikube --help`.

### 5.2 Настройка кластера и тестирование nginx

Последовательность команд для настройки кластера, необходимых аддонов Minikube и всех необходимых k8s-объектов расположена в файле `setup_nginx_k8s.sh`. Для всей необходимой настройки достаточно запустить этот shell-скрипт:

```bash
./setup_nginx_k8s.sh
```

Для обзора кластера можно воспользоваться командой вызова Dashboard и открыть выведенный URL в браузере:

```bash
minikube dashboard --url
```

Для проверки доступности пользовательской страницы nginx, необходимо прописать в файле `/etc/hosts` строку *[IP] my.nginx.web*, где [IP] - IP-адрес, выдаваемый командой `minikube ip`. Либо можно воспользоваться командой:

```bash
echo "$(minikube ip)    my.nginx.web" | sudo tee -a /etc/hosts
```

После этого необходимо проверить возможность открытия web-страницы `http://my.nginx.web` в барузере на локальной машине, где развернут minikube-кластер. Либо можно проверить с помощью команды:

```bash
curl -v http://my.nginx.web
```

Должна отображаться страница с надписью **My own nginx page**.

### 5.3 Очистка ресурсов

Для очистки ресурсов предназначен скрипт `cleanup_nginx_k8s.sh`. Для очистки всех ресурсов кластера и удаления minikube-кластера достаточно выполнить этот скрипт:

```bash
./cleanup_nginx_k8s.sh
```

Также можно удалить ранее прописанную строку в файле `/etc/hosts`.
