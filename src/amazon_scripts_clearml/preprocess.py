import re
import sys
from pathlib import Path
from typing import Any, Dict

import click
import pandas as pd
from nltk import download as nltk_download
from nltk.corpus import stopwords
from nltk.stem import WordNetLemmatizer

ROOT_DIR = Path(__file__).parents[2].resolve()
if str(ROOT_DIR) not in sys.path:
    sys.path.append(str(ROOT_DIR))

from src.amazon_scripts_clearml.hydra_compose_lib import compose_hydra_config  # noqa: E402

nltk_download('stopwords')
nltk_download('wordnet')

URL_PATTERN = re.compile(r'https?://\S+|www\.\S+|\[.*?\]|[^a-zA-Z\s]+|\w*\d\w*')
SPEC_CHARS_PATTERN = re.compile('[0-9 \-_]+')
NON_ALPHA_PATTERN = re.compile('[^a-z A-Z]+')
STOP_WORDS = set(stopwords.words('english'))


def process_text(input_text: str) -> str:
    text = input_text.lower()
    text = URL_PATTERN.sub('', text)
    text = SPEC_CHARS_PATTERN.sub(' ', text)
    text = NON_ALPHA_PATTERN.sub(' ', text)
    text = ' '.join(word for word in text.split() if word not in STOP_WORDS)
    return text.strip()


def tokenize(df: pd.DataFrame, col_name: str, res_col_name: str) -> pd.DataFrame:
    df[res_col_name] = df[col_name].apply(process_text)
    return df


def lemmatize(df: pd.DataFrame, col_name: str, res_col_name: str) -> pd.DataFrame:
    lemmatizer = WordNetLemmatizer()

    df[res_col_name] = df[col_name].apply(
        lambda row: ' '.join(lemmatizer.lemmatize(token) for token in row.split(' '))
    )
    return df


def preprocess_data(raw_df: pd.DataFrame, col_name: str) -> pd.DataFrame:
    res_df = tokenize(raw_df, col_name, 'tokens')
    res_df = lemmatize(res_df, 'tokens', 'corpus')
    return res_df.drop(columns=[col_name, 'tokens'])


@click.command()
@click.argument('raw_data_path', type=Path)
@click.argument('preprocessed_data_path', type=Path)
def main(raw_data_path: Path, preprocessed_data_path: Path):
    """Runs Amazon Reviews data preprocessing and saves the result.

    Args:
        raw_data_path (Path): A Path to raw train.csv data of Amazon Reviews dataset.
        preprocessed_data_path (Path): A path to the output preprocessed data.
    """
    cfg: Dict[str, Any] = compose_hydra_config()['preprocess']

    preprocessed_data_path.parent.mkdir(parents=True, exist_ok=True)

    raw_df = pd.read_csv(
        raw_data_path,
        header=None,
        names=['polarity', 'title', 'review'],
        nrows=cfg['rows_to_read'],
    )
    preprocessed_df = preprocess_data(raw_df, 'review')
    preprocessed_df.to_parquet(preprocessed_data_path)


if __name__ == '__main__':
    main()
