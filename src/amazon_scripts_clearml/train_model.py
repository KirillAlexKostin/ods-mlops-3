import pickle
import sys
from enum import Enum
from pathlib import Path
from typing import Any, Dict, Type

import click
import numpy as np
import scipy as sp
from sklearn.base import BaseEstimator
from sklearn.ensemble import RandomForestClassifier
from sklearn.linear_model import LogisticRegression

ROOT_DIR = Path(__file__).parents[2].resolve()
if str(ROOT_DIR) not in sys.path:
    sys.path.append(str(ROOT_DIR))

from src.amazon_scripts_clearml.hydra_compose_lib import compose_hydra_config  # noqa: E402


class ModelNames(Enum):
    LOGISTIC_REGRESSION = 'logistic_regression'
    RANDOM_FOREST_CLASSIFIER = 'random_forest_classifier'


def train_sklearn_model(
    model_class: Type[BaseEstimator],
    model_params: Dict[str, Any],
    features: np.ndarray,
    target: np.ndarray,
) -> BaseEstimator:
    model = model_class(**model_params)
    return model.fit(features, target)


def train_lr(
    features: np.ndarray, target: np.ndarray, params: Dict[str, Any]
) -> LogisticRegression:
    lr = train_sklearn_model(LogisticRegression, params, features, target)

    # To make the model reproducible
    lr.coef_ = np.round(lr.coef_, decimals=6)
    lr.intercept_ = np.round(lr.intercept_, decimals=6)

    return lr


def train_rfc(
    features: np.ndarray, target: np.ndarray, params: Dict[str, Any]
) -> RandomForestClassifier:
    rfc = train_sklearn_model(RandomForestClassifier, params, features, target)
    return rfc


@click.command()
@click.argument('features_path', type=Path)
@click.argument('target_path', type=Path)
@click.argument('model_path', type=Path)
def main(features_path: Path, target_path: Path, model_path: Path):
    """Runs model training and saves it.

    Args:
        features_path (Path): A path to training features file in npz format.
        target_path (Path): A path to training target file in npy format.
        model_path (Path): A path to output trained model in pkl format.
    """
    cfg: Dict[str, Any] = compose_hydra_config()['model']

    model_name = cfg['active_model']
    if model_name not in ModelNames._value2member_map_:
        raise ValueError(f'Unsupported model name {model_name}')
    supported_model_name = ModelNames(model_name)
    model_params = cfg[model_name]

    model_path.parent.mkdir(parents=True, exist_ok=True)

    features = sp.sparse.load_npz(features_path)
    target = np.load(target_path)
    if supported_model_name == ModelNames.LOGISTIC_REGRESSION:
        model = train_lr(features, target, model_params)
    elif supported_model_name == ModelNames.RANDOM_FOREST_CLASSIFIER:
        model = train_rfc(features, target, model_params)
    pickle.dump(model, model_path.open(mode='wb'))


if __name__ == '__main__':
    main()
