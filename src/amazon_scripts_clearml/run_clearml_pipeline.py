import sys
from pathlib import Path
from typing import Any, Dict, Tuple

import click
import pandas as pd
from clearml import PipelineDecorator, Task
from omegaconf import OmegaConf

ROOT_DIR = Path(__file__).parents[2].resolve()
if str(ROOT_DIR) not in sys.path:
    sys.path.append(str(ROOT_DIR))

# from src.amazon_scripts_clearml.hydra_compose_lib import compose_hydra_config  # noqa: E402
# from src.amazon_scripts_clearml.preprocess import preprocess_data  # noqa: E402
# from src.amazon_scripts_clearml.split import split  # noqa: E402


@PipelineDecorator.component(cache=False, return_values=['preprocessed_data'])
def preprocess_stage(cfg: Dict[str, Any], raw_data_path: Path) -> pd.DataFrame:
    from preprocess import preprocess_data

    raw_df = pd.read_csv(
        raw_data_path,
        header=None,
        names=['polarity', 'title', 'review'],
        nrows=cfg['rows_to_read'],
    )
    return preprocess_data(raw_df, 'review')


@PipelineDecorator.component(cache=False, return_values=['train_df', 'test_df'])
def split_stage(
    cfg: Dict[str, Any], preprocessed_data_path: Path
) -> Tuple[pd.DataFrame, pd.DataFrame]:
    from split import split

    preprocessed_df = pd.read_parquet(preprocessed_data_path)
    train_df, test_df = split(preprocessed_df, cfg)
    return train_df, test_df


# def train_encoder_stage(cfg: Dict[str, Any], train_data_path: Path) -> TfidfVectorizer:
#     train_df = pd.read_parquet(train_data_path)
#     tfidf = train_tfidf(train_df, 'corpus', cfg)
#     return tfidf


# def encode_stage(
#     encoder_path: Path, train_data_path: Path, test_data_path: Path
# ) -> Tuple[np.ndarray, np.ndarray, np.ndarray, np.ndarray]:
#     tfidf = load_sklearn_tfidfvectorizer(encoder_path)

#     train_data_df = pd.read_parquet(train_data_path)
#     train_features = apply_encoder(tfidf, train_data_df, 'corpus')
#     train_target = train_data_df['polarity'].values

#     test_data_df = pd.read_parquet(test_data_path)
#     test_features = apply_encoder(tfidf, test_data_df, 'corpus')
#     test_target = test_data_df['polarity'].values

#     return train_features, train_target, test_features, test_target


# def train_model_stage(cfg: Dict[str, Any], features_path: Path, target_path: Path) -> BaseEstimator:
#     model_name = cfg['active_model']
#     if model_name not in ModelNames._value2member_map_:
#         raise ValueError(f'Unsupported model name {model_name}')
#     supported_model_name = ModelNames(model_name)
#     model_params = cfg[model_name]

#     features = sp.sparse.load_npz(features_path)
#     target = np.load(target_path)
#     if supported_model_name == ModelNames.LOGISTIC_REGRESSION:
#         model = train_lr(features, target, model_params)
#     elif supported_model_name == ModelNames.RANDOM_FOREST_CLASSIFIER:
#         model = train_rfc(features, target, model_params)

#     return model


# def evaluate_stage(
#     model_path: Path, features_path: Path, target_path: Path
# ) -> Tuple[Dict[str, Dict[str, float]], Figure]:
#     features = sp.sparse.load_npz(features_path)
#     target = np.load(target_path)
#     model = pickle.load(model_path.open(mode='rb'))
#     metrics, fig = evaluate(model, features, target)
#     return metrics, fig


@PipelineDecorator.pipeline(project='ODS MLOps Amazon Reviews', name='First Pipeline', version=0.1)
def run_pipeline(cfg: Dict[str, Any], raw_data_path: Path):
    Task.current_task().connect_configuration(name='cfg', configuration=cfg)

    preprocessed_df = preprocess_stage(cfg['preprocess'], raw_data_path)
    preprocessed_data_path = raw_data_path.parent / 'preprocessed/preprocessed.parquet'
    preprocessed_data_path.parent.mkdir(parents=True, exist_ok=True)
    preprocessed_df.to_parquet(preprocessed_data_path)

    train_df, test_df = split_stage(cfg['split'], preprocessed_data_path)
    train_data_path = raw_data_path.parent / 'splitted/train.parquet'
    test_data_path = raw_data_path.parent / 'splitted/test.parquet'
    train_data_path.parent.mkdir(parents=True, exist_ok=True)
    test_data_path.parent.mkdir(parents=True, exist_ok=True)
    train_df.to_parquet(train_data_path)
    test_df.to_parquet(test_data_path)


@click.command()
@click.argument('raw_data_path', type=Path)
def main(raw_data_path: Path):
    """Runs the whole ClearML experiment.

    Args:
        raw_data_path (Path): A Path to raw train.csv data of Amazon Reviews dataset.
    """
    from hydra_compose_lib import compose_hydra_config

    cfg: Dict[str, Any] = OmegaConf.to_object(compose_hydra_config())

    PipelineDecorator.run_locally()
    run_pipeline(cfg, raw_data_path)
    # task = Task.init(
    #     project_name='ODS MLOps Amazon Reviews',
    #     task_name=cfg['model']['active_model'],
    #     output_uri=True,
    # )
    # logger = task.get_logger()

    # # TODO(KostinKA): Experiment with the configuration using agent
    # task.connect_configuration(name='cfg', configuration=OmegaConf.to_object(cfg))

    # task.set_progress(0)

    # # Preprocessing
    # preprocessed_df = preprocess_stage(cfg['preprocess'], raw_data_path)

    # preprocessed_data_path = raw_data_path.parent / 'preprocessed/preprocessed.parquet'
    # preprocessed_data_path.parent.mkdir(parents=True, exist_ok=True)
    # preprocessed_df.to_parquet(preprocessed_data_path)

    # task.set_progress(20)

    # # Splitting
    # train_df, test_df = split_stage(cfg['split'], preprocessed_data_path)

    # train_data_path = raw_data_path.parent / 'splitted/train.parquet'
    # test_data_path = raw_data_path.parent / 'splitted/test.parquet'
    # train_data_path.parent.mkdir(parents=True, exist_ok=True)
    # test_data_path.parent.mkdir(parents=True, exist_ok=True)
    # train_df.to_parquet(train_data_path)
    # test_df.to_parquet(test_data_path)

    # task.set_progress(40)

    # # Encoder training
    # tfidf = train_encoder_stage(cfg['tfidf'], train_data_path)

    # encoder_path = raw_data_path.parent / 'encoder/tfidf.pkl'
    # encoder_path.parent.mkdir(parents=True, exist_ok=True)
    # dump_sklearn_tfidfvectorizer(tfidf, encoder_path)

    # task.set_progress(60)

    # # Data encoding
    # train_features, train_target, test_features, test_target = encode_stage(
    #     encoder_path, train_data_path, test_data_path
    # )

    # train_encoded_features_path = raw_data_path.parent / 'encoded/train_features.npz'
    # train_target_path = raw_data_path.parent / 'encoded/train_target.npy'
    # train_encoded_features_path.parent.mkdir(parents=True, exist_ok=True)
    # train_target_path.parent.mkdir(parents=True, exist_ok=True)
    # sp.sparse.save_npz(train_encoded_features_path, train_features)
    # np.save(train_target_path, train_target)

    # test_encoded_features_path = raw_data_path.parent / 'encoded/test_features.npz'
    # test_target_path = raw_data_path.parent / 'encoded/test_target.npy'
    # test_encoded_features_path.parent.mkdir(parents=True, exist_ok=True)
    # test_target_path.parent.mkdir(parents=True, exist_ok=True)
    # sp.sparse.save_npz(test_encoded_features_path, test_features)
    # np.save(test_target_path, test_target)

    # task.set_progress(80)

    # # Model training
    # model = train_model_stage(cfg['model'], train_encoded_features_path, train_target_path)

    # model_path = raw_data_path.parent / 'models/model.pkl'
    # model_path.parent.mkdir(parents=True, exist_ok=True)
    # pickle.dump(model, model_path.open(mode='wb'))

    # task.set_progress(90)

    # # Metrics evaluation
    # metrics, fig = evaluate_stage(model_path, test_encoded_features_path, test_target_path)
    # logger.report_single_value('accuracy', metrics.pop('accuracy'))
    # for class_name, metrics in metrics.items():
    #     for metric, value in metrics.items():
    #         if metric == 'support':
    #             continue
    #         logger.report_single_value(f'{class_name}_{metric}', value)
    # logger.report_matplotlib_figure(title='Confusion Matrix', series='Confusion Matrix', figure=fig)

    # metrics_path = raw_data_path.parent / 'metrics/cls_report.json'
    # plot_path = raw_data_path.parent / 'metrics/conf_matrix.png'
    # metrics_path.parent.mkdir(parents=True, exist_ok=True)
    # plot_path.parent.mkdir(parents=True, exist_ok=True)
    # json.dump(metrics, metrics_path.open(mode='w'))
    # plt.savefig(plot_path)

    # task.set_progress(100)


if __name__ == '__main__':
    main()
