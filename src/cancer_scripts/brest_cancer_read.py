"""The script runs cancer data reading and filtering."""

import sys
from pathlib import Path

import hydra
import pandas as pd
import phik  # noqa: F401
from hydra.core.config_store import ConfigStore
from omegaconf import OmegaConf

ROOT_DIR = Path(__file__).parents[2]
if str(ROOT_DIR) not in sys.path:
    sys.path.append(str(ROOT_DIR))

from src.cancer_scripts.config import DataReadingConfig, PhikFilterConfig  # noqa: E402

cs = ConfigStore.instance()
cs.store(name='config', node=DataReadingConfig)
cs.store(group='filter', name='phik_filter', node=PhikFilterConfig)


@hydra.main(version_base=None, config_name='config')
def main(cfg: DataReadingConfig):
    print(OmegaConf.to_yaml(cfg))

    df = pd.read_csv(ROOT_DIR / cfg.data.raw_data_path)

    phik_corr = df.phik_matrix()
    phik_corr = phik_corr.loc[phik_corr['diagnosis'] > cfg.filter.target_corr_threshold].drop(
        index=cfg.filter.corr_features
    )

    df = df[list(phik_corr.index)]

    df.to_csv(ROOT_DIR / cfg.data.read_data_path, index=False)


if __name__ == '__main__':
    main()
