"""The script runs read cancer data preprocessing for ML models training."""

import sys
from pathlib import Path

import hydra
import pandas as pd
from hydra.core.config_store import ConfigStore
from hydra.utils import instantiate
from omegaconf import OmegaConf
from sklearn.model_selection import train_test_split

ROOT_DIR = Path(__file__).parents[2]
if str(ROOT_DIR) not in sys.path:
    sys.path.append(str(ROOT_DIR))


from src.cancer_scripts.config import PreprocessingConfig, StandardProcessingConfig  # noqa E402

cs = ConfigStore.instance()
cs.store(name='config', node=PreprocessingConfig)
cs.store(group='processing', name='standard', node=StandardProcessingConfig)


@hydra.main(version_base=None, config_path='.', config_name='config')
def main(cfg: PreprocessingConfig):
    print(OmegaConf.to_yaml(cfg))

    df = pd.read_csv(ROOT_DIR / cfg.data.read_data_path)

    df.loc[:, 'diagnosis'] = df.loc[:, 'diagnosis'].map({'M': 1.0, 'B': 0.0})

    X, y = df.drop(columns=['diagnosis']), df['diagnosis']

    X_train, X_test, y_train, y_test = train_test_split(
        X,
        y,
        test_size=cfg.processing.test_size_ratio,
        random_state=cfg.processing.random_seed,
        stratify=y,
    )

    if cfg.processing.standardize_data:
        ss = instantiate(cfg.standard_scaler)
        ss.fit(X_train)

        X_train = pd.DataFrame(
            data=ss.transform(X_train), index=X_train.index, columns=X_train.columns
        )
        X_test = pd.DataFrame(ss.transform(X_test), index=X_test.index, columns=X_test.columns)

    x_train_path = cfg.data.x_train_path.format(
        std_mode=('STD' if cfg.processing.standardize_data else '')
    )
    X_train.to_csv(ROOT_DIR / x_train_path, index=False)
    x_test_path = cfg.data.x_test_path.format(
        std_mode=('STD' if cfg.processing.standardize_data else '')
    )
    X_test.to_csv(ROOT_DIR / x_test_path, index=False)
    y_train_path = cfg.data.y_train_path.format(
        std_mode=('STD' if cfg.processing.standardize_data else '')
    )
    y_train.to_csv(ROOT_DIR / y_train_path, index=False)
    y_test_path = cfg.data.y_test_path.format(
        std_mode=('STD' if cfg.processing.standardize_data else '')
    )
    y_test.to_csv(ROOT_DIR / y_test_path, index=False)


if __name__ == '__main__':
    main()
