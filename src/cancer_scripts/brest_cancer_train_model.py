"""The script runs ML models training of preprocessed cancer data."""

import pickle
import sys
from pathlib import Path

import hydra
import pandas as pd
from hydra.core.config_store import ConfigStore
from hydra.utils import instantiate
from omegaconf import OmegaConf
from sklearn.metrics import accuracy_score, f1_score, precision_score, recall_score

ROOT_DIR = Path(__file__).parents[2]
if str(ROOT_DIR) not in sys.path:
    sys.path.append(str(ROOT_DIR))

from src.cancer_scripts.config import (  # noqa: E402
    LogregModelTrainingDataConfig,
    LogregSklearnModelConfig,
    ModelTrainingConfig,
    RandomforestClsModelTrainingDataConfig,
    RandomforestClsSklearnModelConfig,
)

cs = ConfigStore.instance()
cs.store(name='config', node=ModelTrainingConfig)
cs.store(group='data', name='logreg', node=LogregModelTrainingDataConfig)
cs.store(group='data', name='randomforest', node=RandomforestClsModelTrainingDataConfig)
cs.store(group='model', name='logreg', node=LogregSklearnModelConfig)
cs.store(group='model', name='randomforest', node=RandomforestClsSklearnModelConfig)


@hydra.main(version_base=None, config_path='.', config_name='config')
def main(cfg: ModelTrainingConfig):
    print(OmegaConf.to_yaml(cfg))

    X_train = pd.read_csv(ROOT_DIR / cfg.data.x_train_path)
    X_test = pd.read_csv(ROOT_DIR / cfg.data.x_test_path)
    y_train = pd.read_csv(ROOT_DIR / cfg.data.y_train_path)
    y_test = pd.read_csv(ROOT_DIR / cfg.data.y_test_path)

    model = instantiate(cfg.model)

    model.fit(X_train.values, y_train.values.ravel())
    y_pred = model.predict(X_test.values)

    print(f'Accuracy:, {accuracy_score(y_test.values.ravel(), y_pred):.03f}')
    print(f'Precision:, {precision_score(y_test.values.ravel(), y_pred):.03f}')
    print(f'Recall:, {recall_score(y_test.values.ravel(), y_pred):.03f}')
    print(f'F1:, {f1_score(y_test.values.ravel(), y_pred):.03f}')

    with open(str(ROOT_DIR / cfg.data.model_path), 'wb') as file:
        pickle.dump(model, file)


if __name__ == '__main__':
    main()
