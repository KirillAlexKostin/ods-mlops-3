from dataclasses import dataclass, field
from typing import Any, List

from omegaconf import MISSING

# Base configs


@dataclass
class DataConfig:
    raw_data_path: str = 'data/cancer_data/Cancer_Data.csv'
    read_data_path: str = 'data/cancer_data/Cancer_Data_Read.csv'
    x_train_path: str = MISSING
    x_test_path: str = MISSING
    y_train_path: str = MISSING
    y_test_path: str = MISSING


@dataclass
class StandardScalerConfig:
    _target_: str = 'sklearn.preprocessing.StandardScaler'
    _convert_: str = 'object'


# data_reading configs


@dataclass
class FilterConfig:
    target_corr_threshold: float = MISSING
    corr_features: List[str] = MISSING


@dataclass
class PhikFilterConfig(FilterConfig):
    target_corr_threshold: float = 0.5
    corr_features: List[str] = field(
        default_factory=lambda: [
            'radius_mean',
            'area_mean',
            'perimeter_mean',
            'radius_worst',
            'area_worst',
            'concavity_mean',
            'radius_se',
            'compactness_worst',
        ]
    )


@dataclass
class DataReadingConfig:
    defaults: List[Any] = field(
        default_factory=lambda: [
            {'filter': 'phik_filter'},
        ]
    )
    data: DataConfig = field(default_factory=DataConfig)
    filter: FilterConfig = MISSING


# preprocessing configs


@dataclass
class PreprocessingDataConfig(DataConfig):
    x_train_path: str = 'data/cancer_data/Cancer_Data_Xtrain{std_mode}.csv'
    x_test_path: str = 'data/cancer_data/Cancer_Data_Xtest{std_mode}.csv'
    y_train_path: str = 'data/cancer_data/Cancer_Data_ytrain{std_mode}.csv'
    y_test_path: str = 'data/cancer_data/Cancer_Data_ytest{std_mode}.csv'


@dataclass
class ProcessingConfig:
    standardize_data: bool = MISSING
    test_size_ratio: float = 0.25
    random_seed: int = 987


@dataclass
class StandardProcessingConfig(ProcessingConfig):
    standardize_data: bool = True


@dataclass
class PreprocessingConfig:
    defaults: List[Any] = field(default_factory=lambda: ['_self_', {'processing': 'standard'}])
    data: PreprocessingDataConfig = field(default_factory=PreprocessingDataConfig)
    processing: ProcessingConfig = MISSING
    standard_scaler: StandardScalerConfig = field(default_factory=StandardScalerConfig)


# model_training configs


@dataclass
class ModelTrainingDataConfig(DataConfig):
    model_path: str = MISSING


@dataclass
class LogregModelTrainingDataConfig(ModelTrainingDataConfig):
    x_train_path: str = 'data/cancer_data/Cancer_Data_XtrainSTD.csv'
    x_test_path: str = 'data/cancer_data/Cancer_Data_XtestSTD.csv'
    y_train_path: str = 'data/cancer_data/Cancer_Data_ytrainSTD.csv'
    y_test_path: str = 'data/cancer_data/Cancer_Data_ytestSTD.csv'
    model_path: str = 'data/cancer_data/logreg.pkl'


@dataclass
class RandomforestClsModelTrainingDataConfig(ModelTrainingDataConfig):
    x_train_path: str = 'data/cancer_data/Cancer_Data_Xtrain.csv'
    x_test_path: str = 'data/cancer_data/Cancer_Data_Xtest.csv'
    y_train_path: str = 'data/cancer_data/Cancer_Data_ytrain.csv'
    y_test_path: str = 'data/cancer_data/Cancer_Data_ytest.csv'
    model_path: str = 'data/cancer_data/rf_cls.pkl'


@dataclass
class SklearnModelConfig:
    _target_: str = MISSING
    _convert_: str = 'object'
    random_state: int = 987
    n_jobs: int = '${oc.env:SKLEARN_N_JOBS,2}'


@dataclass
class LogregSklearnModelConfig(SklearnModelConfig):
    _target_: str = 'sklearn.linear_model.LogisticRegression'


@dataclass
class RandomforestClsSklearnModelConfig(SklearnModelConfig):
    _target_: str = 'sklearn.ensemble.RandomForestClassifier'


@dataclass
class ModelTrainingConfig:
    defaults: List[Any] = field(
        default_factory=lambda: [
            '_self_',
            {'data': 'logreg'},
            {'model': 'logreg'},
        ]
    )
    data: ModelTrainingDataConfig = MISSING
    model: SklearnModelConfig = MISSING


# main config


@dataclass
class MainConfig:
    defaults: List[Any] = field(
        default_factory=lambda: [
            '_self_',
            {'data_reading': 'phik_filter'},
            {'preprocessing': 'standard'},
            {'model_training': 'logreg'},
        ]
    )
    data_reading: DataReadingConfig = MISSING
    preprocessing: PreprocessingConfig = MISSING
    model_training: ModelTrainingConfig = MISSING
