from pathlib import Path
from typing import Any, Dict, Tuple

import click
import dvc.api
import pandas as pd
from omegaconf import DictConfig
from sklearn.model_selection import train_test_split


def split(df: pd.DataFrame, cfg: DictConfig) -> Tuple[pd.DataFrame, pd.DataFrame]:
    return train_test_split(
        df,
        test_size=cfg['test_ratio'],
        shuffle=cfg['shuffle'],
        stratify=df[cfg['stratify_column']] if 'stratify_column' in cfg else None,
        random_state=cfg['random_state'],
    )


@click.command()
@click.argument('preprocessed_data_path', type=Path)
@click.argument('train_data_path', type=Path)
@click.argument('test_data_path', type=Path)
def main(preprocessed_data_path: Path, train_data_path: Path, test_data_path: Path):
    """Runs Amazon Reviews preprocessed data splitting.

    Args:
        preprocessed_data_path (Path): A path to preprocessed data.
        train_data_path (Path): A path to output training data.
        test_data_path (Path): A path to output testing data.
    """
    cfg: Dict[str, Any] = dvc.api.params_show()['split']

    train_data_path.parent.mkdir(parents=True, exist_ok=True)
    test_data_path.parent.mkdir(parents=True, exist_ok=True)

    preprocessed_df = pd.read_parquet(preprocessed_data_path)
    train_df, test_df = split(preprocessed_df, cfg)
    train_df.to_parquet(train_data_path)
    test_df.to_parquet(test_data_path)


if __name__ == '__main__':
    main()
