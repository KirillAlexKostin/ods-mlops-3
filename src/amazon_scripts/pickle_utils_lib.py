import pickle
from pathlib import Path

from sklearn.feature_extraction.text import TfidfVectorizer


def dump_sklearn_tfidfvectorizer(tfidf: TfidfVectorizer, dump_path: Path):
    """Dumps sklearn TfidfVectorizer object as reproducible pickle file.

    Args:
        tfidf (TfidfVectorizer): An object of sklearn TfidfVectorizer class.
        dump_path (Path): A path to dump vectorizer on disk.
    """
    tfidf.stop_words_ = sorted(tfidf.stop_words_)
    tfidf.vocabulary_ = dict(sorted(tfidf.vocabulary_.items(), key=lambda x: x[1]))
    tfidf._stop_words_id = 0

    pickle.dump(tfidf, dump_path.open(mode='wb'))


def load_sklearn_tfidfvectorizer(load_path: Path) -> TfidfVectorizer:
    """Reproducibly loads sklearn TfidfVectorizer obkect from pickle file.

    Args:
        load_path (Path): A path to pickle file.

    Returns:
        TfidfVectorizer: A loaded TfidfVectorizer object.
    """
    tfidf = pickle.load(load_path.open(mode='rb'))
    tfidf.stop_words_ = set(tfidf.stop_words_)

    return tfidf
