import sys
from pathlib import Path

import click
import numpy as np
import pandas as pd
import scipy as sp
from sklearn.feature_extraction.text import TfidfVectorizer

ROOT_DIR = Path(__file__).parents[2].resolve()
if str(ROOT_DIR) not in sys.path:
    sys.path.append(str(ROOT_DIR))

from src.amazon_scripts.pickle_utils_lib import load_sklearn_tfidfvectorizer  # noqa: E402


def apply_encoder(tfidf: TfidfVectorizer, df: pd.DataFrame, col_name: str) -> np.ndarray:
    return np.round(tfidf.transform(df[col_name].values), decimals=6)


@click.command()
@click.argument('encoder_path', type=Path)
@click.argument('data_path', type=Path)
@click.argument('encoded_features_path', type=Path)
@click.argument('target_path', type=Path)
def main(
    encoder_path: Path,
    data_path: Path,
    encoded_features_path: Path,
    target_path: Path,
):
    """Runs data encoding using trained encoder.

    Args:
        encoder_path (Path): A Path to trained encoder in .pkl format.
        data_path (Path): A path to data.
        encoded_features_path (Path): A path to encoded data containing only features in npz format.
        target_path (Path): A path to data containing only taget in npy format.
    """
    encoded_features_path.parent.mkdir(parents=True, exist_ok=True)
    target_path.parent.mkdir(parents=True, exist_ok=True)

    tfidf = load_sklearn_tfidfvectorizer(encoder_path)
    data_df = pd.read_parquet(data_path)
    features = apply_encoder(tfidf, data_df, 'corpus')
    sp.sparse.save_npz(encoded_features_path, features)
    np.save(target_path, data_df['polarity'].values)


if __name__ == '__main__':
    main()
