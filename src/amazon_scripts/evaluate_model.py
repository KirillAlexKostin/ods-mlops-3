import json
import pickle
from pathlib import Path
from typing import Dict, Tuple

import click
import numpy as np
import scipy as sp
from matplotlib import pyplot as plt
from matplotlib.figure import Figure
from sklearn.base import BaseEstimator
from sklearn.metrics import (
    ConfusionMatrixDisplay,
    classification_report,
)


def conf_matrix(y_true: np.ndarray, pred: np.ndarray) -> Figure:
    plt.ioff()
    fig, ax = plt.subplots(figsize=(5, 5))
    ConfusionMatrixDisplay.from_predictions(y_true, pred, ax=ax, colorbar=False)
    ax.xaxis.set_tick_params(rotation=90)
    _ = ax.set_title('Confusion Matrix')
    plt.tight_layout()
    return fig


def evaluate(
    model: BaseEstimator, features: np.ndarray, target: np.ndarray
) -> Tuple[Dict[str, Dict[str, float]], Figure]:
    predicts = model.predict(features)
    fig = conf_matrix(target, predicts)
    return classification_report(target, predicts, output_dict=True), fig


@click.command()
@click.argument('test_features_path', type=Path)
@click.argument('test_target_path', type=Path)
@click.argument('model_path', type=Path)
@click.argument('metrics_path', type=Path)
@click.argument('plot_path', type=Path)
def main(
    test_features_path: Path,
    test_target_path: Path,
    model_path: Path,
    metrics_path: Path,
    plot_path: Path,
):
    """Runs trained model evaluation.

    Args:
        test_features_path (Path): A path to test features path in npz format.
        test_target_path (Path): A path to test target path in npy format.
        model_path (Path): A path to trained model in pkl format.
        metrics_path (Path): A path to file to save calculated metrics in json format.
        plot_path (Path): A path to file to save confusion matrix as an image in png format.
    """
    metrics_path.parent.mkdir(parents=True, exist_ok=True)
    plot_path.parent.mkdir(parents=True, exist_ok=True)

    features = sp.sparse.load_npz(test_features_path)
    target = np.load(test_target_path)
    model = pickle.load(model_path.open(mode='rb'))
    metrics, fig = evaluate(model, features, target)
    json.dump(metrics, metrics_path.open(mode='w'))
    plt.savefig(plot_path)


if __name__ == '__main__':
    main()
