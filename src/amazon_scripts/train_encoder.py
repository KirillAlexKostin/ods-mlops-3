import sys
from pathlib import Path
from typing import Any, Dict

import click
import dvc.api
import pandas as pd
from omegaconf import DictConfig
from sklearn.feature_extraction.text import TfidfVectorizer

ROOT_DIR = Path(__file__).parents[2].resolve()
if str(ROOT_DIR) not in sys.path:
    sys.path.append(str(ROOT_DIR))

from src.amazon_scripts.pickle_utils_lib import dump_sklearn_tfidfvectorizer  # noqa: E402


def train_tfidf(df: pd.DataFrame, col_name: str, tfidf_params: DictConfig) -> TfidfVectorizer:
    tfidf_vectorizer = TfidfVectorizer(**tfidf_params)
    return tfidf_vectorizer.fit(df[col_name].values)


@click.command()
@click.argument('train_data_path', type=Path)
@click.argument('encoder_path', type=Path)
def main(train_data_path: Path, encoder_path: Path):
    """Trains TF-IDF vectorizer and saves it.

    Args:
        train_data_path (Path): A path to training data.
        encoder_path (Path): A path to output TF-IDF vectorized file.
    """
    # TODO: Replace hydra config with dvc.api.params_show()
    cfg: Dict[str, Any] = dvc.api.params_show()['tfidf']

    encoder_path.parent.mkdir(parents=True, exist_ok=True)

    train_df = pd.read_parquet(train_data_path)
    tfidf = train_tfidf(train_df, 'corpus', cfg)
    dump_sklearn_tfidfvectorizer(tfidf, encoder_path)


if __name__ == '__main__':
    main()
