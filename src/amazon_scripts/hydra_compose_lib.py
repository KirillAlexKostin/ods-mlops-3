from typing import Any, Dict, List

from hydra import compose, initialize


def compose_hydra_config(
    overrides: List[str] | None = None,
    config_path: str = '.',
    config_name: str = 'config',
) -> Dict[str, Any]:
    with initialize(version_base=None, config_path=config_path):
        hydra_config = compose(config_name=config_name, overrides=overrides)
        return hydra_config
