import re
import sys
from pathlib import Path

import pytest
from hypothesis import given
from hypothesis import strategies as st

ROOT_DIR = Path(__file__).parents[2].resolve()
if str(ROOT_DIR) not in sys.path:
    sys.path.append(str(ROOT_DIR))

from src.amazon_scripts.preprocess import process_text, process_text_with_none  # noqa E402
from src.amazon_scripts.preprocess import (  # noqa E402
    URL_PATTERN,
    NON_ALPHA_PATTERN,
    STOP_WORDS,
)

UPPERCASE_PATTERN = re.compile(r'[A-Z]+')
SPEC_CHARS_PATTERN = re.compile(r'[0-9\-_]+')


@pytest.mark.xfail(reason='Should fail')
@given(st.text() | st.none())
def test_process_text_none_fail(input_text: str | None):
    output_text = process_text(input_text)
    if input_text is None:
        assert output_text is None
    else:
        assert output_text is not None


@given(st.text() | st.none())
def test_process_text_none(input_text: str | None):
    output_text = process_text_with_none(input_text)
    if input_text is None:
        assert output_text is None
    else:
        assert output_text is not None


@given(st.text())
def test_process_text_lowwercase(input_text: str):
    output_text = process_text(input_text)
    assert '+' not in UPPERCASE_PATTERN.sub('+', output_text)


@given(st.text())
def test_process_text_url(input_text: str):
    output_text = process_text(input_text)
    assert '+' not in URL_PATTERN.sub('+', output_text)


@given(st.text())
def test_process_text_specchars(input_text: str):
    output_text = process_text(input_text)
    assert '+' not in SPEC_CHARS_PATTERN.sub('+', output_text)


@given(st.text())
def test_process_text_nonalpha(input_text: str):
    output_text = process_text(input_text)
    assert '+' not in NON_ALPHA_PATTERN.sub('+', output_text)


@given(st.text())
def test_process_text_stopwords(input_text: str):
    output_text = process_text(input_text)
    for word in output_text.split():
        assert word not in STOP_WORDS


@given(st.text())
def test_process_text_strip(input_text: str):
    output_text = process_text(input_text)
    assert len(output_text) == len(output_text.strip())


@given(st.text())
def test_process_test_oracle(input_text: str):
    assert process_text(input_text) == process_text_with_none(input_text)
