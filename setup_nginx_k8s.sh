#!/bin/bash

minikube start --cpus=4 --memory=16gb --disk-size=20gb

minikube addons enable dashboard
minikube addons enable metrics-server
minikube addons enable ingress

minikube kubectl -- --help

minikube kubectl -- apply -f k8s/nginx-configmap.yaml -n default
sleep 1
minikube kubectl -- apply -f k8s/nginx-deployment.yaml -n default
sleep 1
minikube kubectl -- apply -f k8s/nginx-service.yaml -n default
sleep 10
minikube kubectl -- apply -f k8s/nginx-ingress.yaml -n default
