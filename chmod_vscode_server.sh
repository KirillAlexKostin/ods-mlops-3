#!/bin/bash

VSCODE_SERVER_FOLDER="/home/ods-user/.vscode-server"
while getopts ":s:" opt; do
  case $opt in
    s) vscode_server_folder="$OPTARG"
      if [ "$vscode_server_folder" != "" ]; then
        VSCODE_SERVER_FOLDER="$vscode_server_folder"
      fi
      ;;
    \?) echo "Invalid option -$OPTARG" >&2
    ;;
  esac
done


sudo chown -R $(id -u) "$VSCODE_SERVER_FOLDER"
sudo chgrp -R $(id -g) "$VSCODE_SERVER_FOLDER"

exec "$@"
